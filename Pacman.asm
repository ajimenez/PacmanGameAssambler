include emu8086.inc
include macros.asm 

.MODEL small
.STACK 50h    ; Segmento de pila
   
    DW 256 DUP (?)

.DATA                           
    ; Segmento para declarar variable de datos
    

    ;---------------------  files location --------------------

      stadisticsFile db "C:\stadisticsFile.txt",0
      buf db ?
    ;----------------------------------------------------------

    ;---------- vars to catch the execution time ---------------

        SLInit DB ?
        SHInit DB ?
        MLInit DB ?
        MHInit DB ?
        HLInit DB ?   
        HHInit DB ?

        SLFinish DB ?
        SHFinish DB ?
        MLFinish DB ?
        MHFinish DB ?
        HLFinish DB ?   
        HHFinish DB ?

        HInit db '  '
        MInit db '  '
        SInit db '  '

        HFinish db '  '
        MFinish db '  '
        SFinish db '  '

    ;------------------------------------------------------------

      chartsToUse db 0

      numberOfLife db 0
      points db 0

      newL DB 10,13,'$'
      userName DB '          ','$'
      userName_size = $ - offset userName

      enter db 10, 13, '$'   

      ;10h - trianguloD : resta
      ;11h - trianguloI : suma
      ;24h - dolar : resta
      ;9fh - f rara : suma

      ;06h - picas : resta puntos
      ;05h - trebol : resta puntos 
      ;04h - rombo : suma puntos
      ;0eh - corchea : suma puntos


    ;------------ << Vars for the Character >> --------------------
      decimalBase db 10

      characterRead db 01h
      

      charIntoScreen db 0
                                                 
      xToCheck db 21
      yToCheck db 24   
                          
      posX    db 21
      posY    db 23
      xBefore db 21
      yBefore db 23

      playerMovements db 0

      dlValues db 0
      
    ;------------<<Vars for Files>>--------------------------------
    
      archivo db "c:\Partidas.txt", 0
      handle dw ?
      partida db '                    ','$'
      partida_size = $ - offset partida 
      cargPart db '                   ','$'
      

.CODE 

  jmp START
     
    include procedures.asm

  START:
             
    ;------------------------ Inicializar Segmento de Datos ---------------------------
        
      mov ax, @data
      mov ds, ax    


      programStart:

          clearDesktop                                          ; clear the screen because we run this in a Dosbox

          set_Video_mode_80x25_03h                              ; Macro to set a video mode in 80x25 "Text mode"
          
        ;-------------------------- Draw Menu -----------------

          call displayFirstMenu  
        ;------------------------------------------------------

          chooseAgain:    ; if the user miss the option

          printLocation "Choose your option: ", 22, 1

          call input

          cmp al, 31h                                           ; compare the input char with a null
            je call playMenuWithNameInputProc
            
          CMP AL, 32h
            JE statics
          jne chooseAgain


        ;---------------- Select save game or new -------------
          nextStep:

          ;-------------------------- Draw Menu -----------------

            call displaySecondMenu  
          ;------------------------------------------------------

        chooseAgain2:
        clearDesktop

        

        printLocation "PRESS 1 FOR NEW GAME OR 2 FOR A SAVED GAME", 22, 1

          ;choose between a new game or an old game     
            
            call input

          ;compare the selection      
            CMP AL, 31h
              JE newGame 
            CMP AL, 32h  
              JE oldGame
            JNE chooseAgain2
           
          newGame:
              
              clearDesktop

              printLocation "How many life you want to use?",22,1
              call input
              
              CMP AL,31h
                JE charterSelect
              CMP AL,32h
                JE charterSelect
              CMP AL,33h
                JE charterSelect
              JNE newGame
              
         charterSelect:
              sub AL,30h
              MOV numberOfLife,AL

              clearDesktop

              printLocation "What character do you want to use?", 22,1
              printLocation "Select : ", 24, 2
               
              call input

              mov  characterRead, al

              mov ah, 03h       ; get cursor position
              mov bh, 00h
              int 10h

              clearDesktop 
              call displayThirdMenu

                printLocation 03h,25,18
                printLocation 3Dh,26,18
                setCursorInPosition 27,18
                mov AL,numberOfLife
                add AL,30h
                printChar AL
                printLocation 0Fh,25,19
                printLocation 3Dh,26,19
                mov AL,points
                add AL,30h
                printChar AL

              mov ah, 02h
              mov bh, 00h
              mov dh, 1
              mov dl, 20
              int 10h

              
              ;-------------------------- Catch init time ---------------------
                  push ax
                  push dx
                  push cx

                  MOV AH,2CH
                  INT 21H

                  call getHoursInit
                  call getMinutesInit
                  call getSecondsInit

                  pop ax
                  pop dx
                  pop cx
              
              partidaGuardada:
              call drawAndSetInitialPoint


              go:
              ;------ same to a do-while ------- 
              
              call setCursorBefPos
              call delCharBefPos
              call setCursorPosSelected
              call showCharacterInScreen
              call savePosActual
              call waitCursor


              loopMoveChar:

                  call comprovePointWin ;cambiar aquí para comprobar que hay 
                  
                  go2:
                  
                  cmp ah, 22h
                  je saveGame
                  cmp ah, 72
                  je call bottomMove
                  cmp ah, 80
                  je call topMove
                  cmp ah, 77
                  je call rightMove
                  cmp ah, 75
                  je call leftMove


                  next3:

                  call setCursorPosSelected

                  ;------------- test lines -----------------

                  call readCharacterIntoScreen
                  call setCursorPosSelected

                  cmp charIntoScreen, 20h
                  jne next2
                  je next
                  ;------------------------------------------

                  next:

                  call setCursorBefPos
                  call delCharBefPos
                  call setCursorPosSelected
                  call showCharacterInScreen
                  call savePosActual
                  call updatePoints ;------------update points
                  call waitCursor
                  inc playerMovements
                  jmp loopMoveChar

                  next2:
                  call cmpCharts
                  call setCursorBefPos
                  call savePosBef
                  call waitCursor
                  jmp loopMoveChar
                  
                  saveGame:
                  crearPart
                  guardarPartArch
                  jmp loopMoveChar
                       



          oldGame:

            clearDesktop
            cargarPartida
            armarPartGuard
            printLocation 03h,25,18
            printLocation 3Dh,26,18
            setCursorInPosition 27,18
            mov AL,numberOfLife
            add AL,30h
            printChar AL
            printLocation 0Fh,25,19
            printLocation 3Dh,26,19
            mov AL,points
            add AL,30h
            printChar AL
            jmp partidaGuardada
            
            

          statics:
            clearDesktop

            ;OPEN THE STADISTICS FILE 
            LEA DX, stadisticsFile

            MOV AL,0
            MOV AH,3DH
            INT 21H
            JC errorOpenFile
            MOV BX,AX  

            MOV CX,1 

          stadisticsReader:
            LEA DX,buf
            MOv AH,3FH          ;ONLY READ FROM FILE
            INT 21H
            CMP AX,0
            JZ fileEnd
            MOV AL,BUF
            MOV AH,0EH
            INT 10H

            JMP stadisticsReader       
               
            errorOpenFile:
            clearDesktop
            printLocation "Theres nothing in the file", 20, 1 
            MOV AH,0
            INT 16H
            ;print emptyFile
            JMP programStart
            ;creamos el archivo

            fileEnd:
            mov ah,3eh
            int 21h

            MOV AH,0
            INT 16H
            JMP programStart

          endProgram
            

  end START   