
;================================ MACROS =========================================        

  setCursorInInitGamePosition macro x, y
      mov dl, x
      mov dh, y

      mov posX, dl
      mov posY, dh
      
      mov ah, 02h
      mov bh, 00h 
      int 10
  endm

  getCursorPosition macro
      mov ah, 03h       ; get cursor position
      mov bh, 00h
      int 10h
  endm



  setCursorInPosition macro x, y
      mov ah, 02h
      mov bh, 00h
      mov dh, y
      mov dl, x
      int 10h

  endm

  printMsg macro msg          ; Macro para imprimir mensajes
      mov ah, 09h
      mov dx, offset msg
      int 21h
  endm       

  printChar macro char          ; Macro para chars
      mov ah, 02h
      mov dl, char
      int 21h
  endm        

  printLocation macro text, x, y
      mov ah, 02h
      mov bh, 00h
      mov dh, y
      mov dl, x
      int 10h

      PRINT text

  endm

  setSectionColorScreenTextMode macro cText, xInit, yInit, xFinal, yFinal
      mov ax, 0600h
      mov bh, cText

      mov ch, yInit 
      mov cl, xInit

      mov dh, yFinal
      mov dl, xFinal

      int 10h
  endm

  endProgram macro
      mov ax, 0c07h
      int 21h
      .exit
  endm 

  set_Video_mode_80x25_03h macro    ; Text mode setting
      mov ah, 00h
      mov al, 03h
      int 10h  
  endm
      
      
  set_Video_mode_80x30_12h macro ; Graphics mode setting
      mov ah, 00h
      mov al, 12h
      int 10h  
  endm    
      

  clearDesktop macro 
      mov ah, 06h
      mov al, 00h

      mov bh, 0fh

      mov ch, 00
      mov cl, 20

      mov dh, 24
      mov dl, 79

      int 10h 
  endm

    ;-------------------------- Menu Macro --------------------------------------

      ;---------------------- Option 1 ------------------------------------------

      playMenuWithNameInput macro userName
        ;---------------------- clean the last menu --------------------
        ; set the chosen option

          setSectionColorScreenTextMode 30h, 00, 00, 19, 24 

          setSectionColorScreenTextMode 5fh, 01, 04, 12, 04
          printLocation "1) PLAY!", 2, 4 
        ;--------------------------------------------------------------

          clearDesktop

          printLocation "Type your Name: ", 22, 1

          ;----------------- GET THE NAME AND SAVE IT-----------------

            MOV CX, 15    ;extencion of the word is 15 charters
            MOV SI, 0
            
            L:
              MOV AH,01h
              INT 21H
              CMP AL,0DH ;compare with an enter key
              JE endE    ;if equals jumpo to the end
              MOV userName[SI],AL    
              INC SI 
            loop L
            endE:

      endm

;-------------------------- macros Archivos----------------------------

    guardarPartArch macro
        ;Limpiar registros
        xor ax, ax
        xor bx, bx
        xor cx, cx
        xor dx, dx
        
        mov ah, 3ch
        mov cx, 0
        mov dx, offset archivo
        int 21h
        jc err
        mov handle, ax
        
        ;guarda en el archivo la partida generada
        mov ah, 40h
        mov bx, handle
        mov dx, offset partida
        mov cx, partida_size
        int 21h
        
        mov ah, 3eh
        mov bx, handle
        int 21h
        err:
    endm
    
    crearPart macro
        ;Limpiar registros
        xor ax, ax
        xor bx, bx
        xor cx, cx
        xor dx, dx
        
        ;guarda los movimientos del jugador
        ;en las 2 primeras posiciones de la
        ;variable partida
        mov si, 0
        mov al, playerMovements
        mov bl, 10
        div bl
        
        add al, 48
        add ah, 48
        mov partida[si], al
        inc si  
        mov partida[si], ah
        inc si
        
        ;guarda el simbolo usado 
        ;en la tercera posicion de la
        ;variable partida
        
;        xor ax, ax
;        mov al, charIntoScreen
;        div bl
;        mov partida[si], al
;        inc si
;        mov partida[si], ah
        mov dl, charIntoScreen
        mov partida[si], dl
        inc si
        
        mov cx, userName_size
        
        mov di,0
        
        ;ciclo que guarda el nombre de usuario
        ciclo:
        mov dl, userName[di]
        mov partida[si], dl
        inc si
        inc di
        loop ciclo
        
        ;guarda la posicion en X 
        ;en la variable
        xor ax, ax
        mov al, posX
        div bl
        add al,48
        add ah,48
        mov partida[si], al
        inc si  
        mov partida[si], ah
        inc si
        
        ;guarda la posicion en Y 
        ;en la variable
        xor ax, ax
        mov al, posY
        div bl
        add al,48
        add ah,48
        mov partida[si], al
        inc si  
        mov partida[si], ah
        inc si
        
        ;guarda los puntos obtenidos 
        ;en la variable

        mov dl, points
        mov partida[si], dl
        inc si
        
        ;guarda las vidas restantes 
        ;en la variable
        mov dl, numberOfLife
        mov partida[si], dl
        
        
    endm
    
    cargarPartida macro
        ;Limpiar registros
        xor ax, ax
        xor bx, bx
        xor cx, cx
        xor dx, dx
        
        ;abre el archivo
        mov ax, 3c00h
        mov cx, 0
        mov dx, offset archivo
        int 21h
        jc errc
        mov handle, ax
        
        mov bx, handle
        mov cx, partida_size
        lea dx, cargPart
        mov ah, 3fh
        int 21h
        mov ah, 3eh
        mov bx, handle
        int 21h
        
        errc:
    endm
    
    armarPartGuard macro
        
        ;Limpiar registros
        xor ax, ax
        xor bx, bx
        xor cx, cx
        xor dx, dx
        
        
        mov si,0
        ;cargar los movimientos
        mov cl, cargPart[si]
        inc si
        mov al, cargPart[si]
        cmp cl,0
        je salidamovs
        movs:
        add al,10
        loop movs
        salidamovs:
        mov playerMovements, al
        
        inc si
        
        ;cargar el simbolo
;        mov cl, cargPart[si]
;        inc si
        mov al, cargPart[si]
;        cmp cl,0
;        je salsimb
;        simbol:
;        add al,10
;        loop simbol
;        salsimb:
        mov charIntoScreen, al
        
        inc si
        
        ;cargar el nombre de usuario
        mov cl, userName_size
        mov di,0
        userN:
        mov dl, cargPart[si]
        mov userName[di], dl
        inc si
        inc di
        loop userN
        
        ;cargar las posiciones
        mov cl, cargPart[si]
        inc si
        mov al, cargPart[si]
        cmp cl,0
        je salidaposx
        posix:
        add al, 10
        loop posix
        salidaposx:
        mov posX, al
        
        inc si
        
        mov cl, cargPart[si]
        inc si
        mov al, cargPart[si]
        cmp cl,0
        je salidaposy
        posiy:
        add al, 10
        loop posiy 
        salidaposy:
        mov posY, al
        
        inc si
        
        mov dl, cargPart[si]
        mov points, dl
        
        inc si
        
        mov dl, cargPart[si]
        mov numberOfLife, dl
        
    endm

;---------------------------------- random macros -----------------------------------
    randomPositions macro
      mov ah,2Ch
      int 21h
      cmp dl,50
      jge second
      call charts1
      jmp endRandom
    second:
      call charts2
    endRandom:
    endm


    
;=============================== END MACROS ============================ 