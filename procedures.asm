;=============================== PROCEDURES ====================================== 

    ;----------------------------- Skin procs --------------------------------------

        printTec proc
        ;T
        setSectionColorScreenTextMode 3ch, 22,2,31,2
        setSectionColorScreenTextMode 3ch, 22,3,31,3
        setSectionColorScreenTextMode 3ch, 25,4,27,4
        setSectionColorScreenTextMode 3ch, 25,5,27,5
        setSectionColorScreenTextMode 3ch, 25,6,27,6
        setSectionColorScreenTextMode 3ch, 25,7,27,7
        setSectionColorScreenTextMode 3ch, 25,8,27,8
        setSectionColorScreenTextMode 3ch, 25,9,27,9
        setSectionColorScreenTextMode 3ch, 25,10,27,10
        setSectionColorScreenTextMode 3ch, 25,11,27,11
        ;e
        setSectionColorScreenTextMode 5fh, 29,5,35,5
        setSectionColorScreenTextMode 5fh, 29,6,35,6
        setSectionColorScreenTextMode 5fh, 29,7,31,7
        setSectionColorScreenTextMode 5fh, 33,7,35,7
        setSectionColorScreenTextMode 5fh, 29,8,35,8
        setSectionColorScreenTextMode 5fh, 29,9,31,9
        setSectionColorScreenTextMode 5fh, 29,10,35,10
        setSectionColorScreenTextMode 5fh, 29,11,35,11
        ;c
        setSectionColorScreenTextMode 4ah, 37,4,43,4
        setSectionColorScreenTextMode 4ah, 37,5,43,5
        setSectionColorScreenTextMode 4ah, 37,6,39,6
        setSectionColorScreenTextMode 4ah, 37,7,39,7
        setSectionColorScreenTextMode 4ah, 37,8,39,8
        setSectionColorScreenTextMode 4ah, 37,9,39,9
        setSectionColorScreenTextMode 4ah, 37,10,43,10
        setSectionColorScreenTextMode 4ah, 37,11,43,11
        ;-
        setSectionColorScreenTextMode 2bh, 41,7,46,7
        setSectionColorScreenTextMode 2bh, 41,8,46,8
        ;s
        setSectionColorScreenTextMode 7dh, 48,5,57,4
        setSectionColorScreenTextMode 7dh, 48,6,57,5
        setSectionColorScreenTextMode 7dh, 48,7,50,6
        setSectionColorScreenTextMode 7dh, 48,7,57,7
        setSectionColorScreenTextMode 7dh, 48,8,57,8
        setSectionColorScreenTextMode 7dh, 55,9,57,9
        setSectionColorScreenTextMode 7dh, 48,10,57,10
        setSectionColorScreenTextMode 7dh, 48,11,57,11
        ;s
        setSectionColorScreenTextMode 6eh, 59,5,68,4
        setSectionColorScreenTextMode 6eh, 59,6,68,5
        setSectionColorScreenTextMode 6eh, 59,7,61,6
        setSectionColorScreenTextMode 6eh, 59,7,68,7
        setSectionColorScreenTextMode 6eh, 59,8,68,8
        setSectionColorScreenTextMode 6eh, 66,9,68,9
        setSectionColorScreenTextMode 6eh, 59,10,68,10
        setSectionColorScreenTextMode 6eh, 59,11,68,11
        ;c
        setSectionColorScreenTextMode 1ch, 70,4,76,4
        setSectionColorScreenTextMode 1ch, 70,5,76,5
        setSectionColorScreenTextMode 1ch, 70,6,72,6
        setSectionColorScreenTextMode 1ch, 70,7,72,7
        setSectionColorScreenTextMode 1ch, 70,8,72,8
        setSectionColorScreenTextMode 1ch, 70,9,72,9
        setSectionColorScreenTextMode 1ch, 70,10,76,10
        setSectionColorScreenTextMode 1ch, 70,11,76,11
        
        ret
        printTec endp

    

        mazePacman proc            ; Init point 21, 1
        
        
printLocation "-----------------------------------------------------------", 20, 0 
printLocation "|            --------          -------       ----- |   |  |", 20, 1
printLocation "| ----------     |__| --------         -----         |    |", 20, 2
printLocation "| |__   ___| |-|         ||    -------       ---- ------- |", 20, 3
printLocation "|    | |         ------- || ---------- ---------- ------- |", 20, 4
printLocation "| -- | | ------- | ____| || | _______| | _______| | ____| |", 20, 5
printLocation "| || | | | --- | | |     -- | |        | |        | |     |", 20, 6
printLocation "| -- | | | |_| | | | ------ | |------- | |------- | | |-| |", 20, 7
printLocation "|    | | | ____| | | |____| |_______ | |_______ | | | |_| |", 20, 8
printLocation "| __ | | | |     | |               | |        | | | |     |", 20, 9
printLocation "| || | | | |____ | |____ __ _______| | _______| | | |____ |", 20, 10
printLocation "| -- --- |_____| |_____| || |________| |________| |_____| |", 20, 11
printLocation "|         -----          ||     |   |     -----           |", 20, 12
printLocation "| ------    |   ----- -- || ---   |   ---   |   ----- ----|", 20, 13
printLocation "|         |   |          ||     -----     |   |           |", 20, 14
printLocation "-----------------------------------------------------------", 20, 15



randomPositions ;--------------------print minas

ret
        mazePacman endp  

        youWin proc
            ;-------------------------- Catch final time ---------------------
                  call getHoursFinish
                  call getMinutesFinish
                  call getSecondsFinish

            clearDesktop
            printLocation " __      __                       __       __ __          __", 20, 0 
            printLocation "/  \    /  |                     /  |  _  /  /  |        / |", 20, 1
            printLocation "$$  \  /$$______  __    __       $$ | / \ $$ $$/ _______ $$|", 20, 2 
            printLocation " $$  \/$$/      \/  |  /  |      $$ |/$  \$$ /  /       \$$|", 20, 3 
            printLocation "  $$  $$/$$$$$$  $$ |  $$ |      $$ /$$$  $$ $$ $$$$$$$  $$|", 20, 4 
            printLocation "   $$$$/$$ |  $$ $$ |  $$ |      $$ $$/$$ $$ $$ $$ |  $$ $$/", 20, 5 
            printLocation "    $$ |$$ \__$$ $$ \__$$ |      $$$$/  $$$$ $$ $$ |  $$ |__", 20, 6  
            printLocation "    $$ |$$    $$/$$    $$/       $$$/    $$$ $$ $$ |  $$ / |", 20, 7 
            printLocation "    $$/  $$$$$$/  $$$$$$/        $$/      $$/$$/$$/   $$/$$/", 20, 8 

            ;----------------------------
              setCursorInPosition 25, 12
              PRINT "Player Movements: "
              call convertToDecimal        ; esto solo funciona con digitos hasta el 99 en decimal
              setCursorInPosition 25, 13
              PRINT "Points: "
              mov al,points
              add al,30h
              printChar al
              setCursorInPosition 25, 14
              PRINT "Lifes: "
              mov al,numberOfLife
              add al,30h
              printChar al
              
            ;---------------------------
              


            endProgram
            

            ret
        youWin endp

        youLose proc
;-------------------------- Catch final time ---------------------
                  call getHoursFinish
                  call getMinutesFinish
                  call getSecondsFinish

            clearDesktop
            printLocation " __      __                 __                              ", 20, 0 
            printLocation "/  \    /  |               /  |                             ", 20, 1
            printLocation "$$  \  /$$_____  __    __  $$ |      _____   ______  _____  ", 20, 2 
            printLocation " $$  \/$$/     \/  |  /  | $$ |     /     \ /      |/     \ ", 20, 3 
            printLocation "  $$  $$/$$$$$  $$ |  $$ | $$ |    /$$$$$  /$$$$$$//$$$$$  |", 20, 4 
            printLocation "   $$$$/$$ | $$ $$ |  $$ | $$ |    $$ | $$ $$     \$$   $$ |", 20, 5 
            printLocation "    $$ |$$ \_$$ $$ \__$$ | $$ |____$$ \_$$ |$$$$$  $$$$$$$/ ", 20, 6  
            printLocation "    $$ |$$   $$/$$    $$/  $$      $$   $$//    $$/$$      |", 20, 7 
            printLocation "    $$/  $$$$$/  $$$$$$/   $$$$$$$/ $$$$$/ $$$$$$/  $$$$$$/ ", 20, 8 
     
            ;----------------------------
              setCursorInPosition 25, 12
              PRINT "Player Movements: "
              call convertToDecimal        ; esto solo funciona con digitos hasta el 99 en decimal
              setCursorInPosition 25, 13
              PRINT "Points: "
              mov al,points
              add al,30h
              printChar al
              setCursorInPosition 25, 14
              PRINT "Lifes: "
              mov al,numberOfLife
              add al,30h
              printChar al

            ;---------------------------

            endProgram

        ret
        youLose endp

        drawAndSetInitialPoint proc            ; Init point 21, 23     Final point 51, 15
            mov xToCheck, 21
            mov yToCheck, 24

            mov posX, 21
            mov posY, 1
            mov xBefore, 21
            mov yBefore, 1
            
            call printTec
            call mazePacman

            ;-------------------- draw point into input  -----------
                setSectionColorScreenTextMode 7ah, 21, 1, 21, 1
                
            jmp go
            ret
        drawAndSetInitialPoint endp                

        comprovePointWin proc
            mov al,points
            add al,30h
            cmp al, 37h
                je win
            mov al,numberOfLife
            add al,30h
            cmp al,30h
                je lose
            jmp go2
            lose:
                call youLose
            win:
                call youWin
            ret
        comprovePointWin endp

    ;----------------------------- Game procs --------------------------------------

        output proc
            mov dl, characterRead
            mov ah, 2h
            int 21h
            ret
        output endp

        input proc
            mov ah, 01h
            int 21h
            ret
        input endp

        convertToDecimal proc 
            mov al, playerMovements

            div decimalBase
            mov dx,ax
            or dx,3030h

            mov ah,02h
            int 21h

            xchg dh,dl
            mov ah,02h
            int 21h  
           
            ret
        convertToDecimal endp 

    ;------------------------- Procs to catch time ---------------------------------

        getHoursInit proc
            mov al, ch
            aam 
            add ax, 3030h
            mov HlInit, al
            mov HHInit, ah
            ret
        getHoursInit endp

        getMinutesInit proc
            mov al, cl
            aam
            add ax, 3030h
            mov MLInit, al
            mov MHInit, ah
            ret
        getMinutesInit endp 

        getSecondsInit proc
            mov al, dh
            aam
            add ax, 3030h
            mov SLInit, al
            mov SHInit, ah
            ret
        getSecondsInit endp

        getHoursFinish proc
            mov al, ch
            aam 
            add ax, 3030h
            mov HLFinish, al
            mov HHFinish, ah
            ret
        getHoursFinish endp

        getMinutesFinish proc
            mov al, cl
            aam
            add ax, 3030h
            mov MLFinish, al
            mov MHFinish, ah
            ret
        getMinutesFinish endp 

        getSecondsFinish proc
            mov al, dh
            aam
            add ax, 3030h
            mov SLFinish, al
            mov SHFinish, ah
            ret
        getSecondsFinish endp


        ;------------- test proc ------

    ;----------------------- Procs to move Character -------------------------------
  
        delBefCharacter proc
            mov  ah, 02h
            mov  dl, 32
            int  21h
            ret
        delBefCharacter endp


        setCursorBefPos proc
            mov ah, 02h
            mov bh, 00h  
            mov dh, yBefore
            mov dl, xBefore
            int 10h
            ret
        setCursorBefPos endp

        delCharBefPos proc
            mov ah, 02h
            mov dl, 20h        ; cod ascii 20h = "Space"
            int 21h
            ret
        delCharBefPos endp

        setCursorPosSelected proc
            mov ah, 02h
            mov bh, 00h 
            mov dh, posY
            mov dl, posX
            int 10h
            ret
        setCursorPosSelected endp              
        
        showCharacterInScreen proc
            mov ah, 02h
            mov dl, characterRead
            int 21h
            ret
        showCharacterInScreen endp

        savePosActual proc
            call setCursorPosSelected
            mov xBefore, dl
            mov yBefore, dh
            ret
        savePosActual endp

        updatePoints proc
            setCursorInPosition 27,18
            mov AL,numberOfLife
            add AL,30h
            printChar AL
            setCursorInPosition 27,19
            mov AL,points
            add AL,30h
            printChar AL
            call setCursorPosSelected
        ret 
        updatePoints endp

        savePosBef proc
            mov posX, dl
            mov posY, dh
            ret
        savePosBef endp


        waitCursor proc
            mov ah, 00h
            int 16h
            ret
        waitCursor endp


        readCharacterIntoScreen proc
            mov ah, 08h
            mov bh, 00h
            int 10h
            mov charIntoScreen, al
            ret 
        readCharacterIntoScreen endp

    ;------------------------------- Moves -----------------------------------------

        leftMove proc
            dec posX
            jmp next3
            ret
        leftMove endp

        rightMove proc
            inc posX
            jmp next3
            ret
        rightMove endp

        topMove proc
            inc posY
            jmp next3
            ret
        topMove endp

        bottomMove proc
            dec posY
            jmp next3
            ret
        bottomMove endp

    ;------------------------ SO procs (Console) ----------------------------------

        cls proc
            
            mov ah, 05h
            mov al, 00h
            int 10h

            ret
        cls endp

    ;-------------------------- Aparience procs ------------------------------------

        playMenuWithNameInputProc proc
            push ax
              playMenuWithNameInput userName
            pop ax
            jmp nextStep
            ret
        playMenuWithNameInputProc endp

        displayFirstMenu proc

              setSectionColorScreenTextMode 30h, 00, 00, 19, 24 

              setSectionColorScreenTextMode 5fh, 01, 04, 12, 04
              printLocation "1) PLAY!", 2, 4 

              setSectionColorScreenTextMode 5fh, 01, 12, 14, 12
              printLocation "2) STADISTICS", 2, 12

              ret

        displayFirstMenu endp

        displaySecondMenu proc

              setSectionColorScreenTextMode 5fh, 03, 06, 13, 06
              printLocation "1.NEW GAME", 4, 6

              setSectionColorScreenTextMode 5fh, 03, 08, 15, 08
              printLocation "2.SAVED GAME", 4, 8

              ret
        displaySecondMenu endp

        displayThirdMenu proc
            ;---------------------- clean the last menu --------------------
              ; set the chosen option

                setSectionColorScreenTextMode 30h, 00, 00, 19, 24 

                setSectionColorScreenTextMode 5fh, 01, 04, 12, 04
                printLocation "1) PLAY!", 2, 4 

                setSectionColorScreenTextMode 5fh, 01, 06, 17, 06
                printLocation "g. Save the game", 1, 6

            ;--------------------------------------------------------------    
              

              ret
        displayThirdMenu endp


        charts1 proc 
            
            setCursorInPosition 25,14
            printChar 9fh
            setCursorInPosition 47,14
            printChar 11h
            setCursorInPosition 38,12
            printChar 11h
            setCursorInPosition 48,09
            printChar 9fh
            setCursorInPosition 23,08
            printChar 9fh
            setCursorInPosition 38,03
            printChar 11h
            setCursorInPosition 22,04
            printChar 10h
            setCursorInPosition 28,08
            printChar 24h
            setCursorInPosition 44,05
            printChar 11h
            setCursorInPosition 64,14
            printChar 24h

        ret
        charts1 endp

        charts2 proc 
            
            setCursorInPosition 28,01
            printChar 04h
            setCursorInPosition 73,01
            printChar 0eh
            setCursorInPosition 64,09
            printChar 0eh
            setCursorInPosition 45,09
            printChar 05h
            setCursorInPosition 73,12
            printChar 04h
            setCursorInPosition 36,07
            printChar 06h
            setCursorInPosition 21,06
            printChar 0eh
            setCursorInPosition 61,03
            printChar 04h
            setCursorInPosition 33,04
            printChar 06h
            setCursorInPosition 65,06
            printChar 04h

        ret
        charts2 endp

        cmpCharts proc
            cmp charIntoScreen, 10h
                je restaVida
            cmp charIntoScreen, 06h
                je restaVida
            cmp charIntoScreen, 24h
                je restaVida
            cmp charIntoScreen, 05h
                je restaVida
            cmp charIntoScreen, 9fh
                je sumaPuntos
            cmp charIntoScreen, 11h
                je sumaPuntos
            cmp charIntoScreen, 04h
                je sumaPuntos
            cmp charIntoScreen, 0eh
                je sumaPuntos
            jmp finalCmp

            restaVida:
            mov al,numberOfLife
            dec al
            mov numberOfLife,al
            jmp next

            sumaPuntos:
            mov al, points
            inc al
            mov points,al
            jmp next

            finalCmp:
        ret
        cmpCharts endp
;============================= END PROCEDURES ====================================                               
