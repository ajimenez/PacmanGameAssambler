;=============================== PROCEDURES ====================================== 

    ;----------------------------- Skin procs --------------------------------------

        mazeDifficult proc            ; Init point 21, 23
            printLocation "-------------------------------", 20, 0 
            printLocation "|                   |       | |", 20, 1
            printLocation "| --------- ------- | --- - | |", 20, 2
            printLocation "| |       |     |   |   | | | |", 20, 3
            printLocation "| | ----- |---- | ------| | | |", 20, 4
            printLocation "| | |   |     | | |     | |   |", 20, 5
            printLocation "|-- | | ----- | | | | | | ----|", 20, 6
            printLocation "|   | |   | | | | | | | |     |", 20, 7
            printLocation "| --- --- | | | | --- | ----- |", 20, 8
            printLocation "| |     | | | | |     |       |", 20, 9
            printLocation "| | --- | | | | |------------ |", 20, 10
            printLocation "| | | | |   | | |           | |", 20, 11
            printLocation "| | | | ----| | | --- ------- |", 20, 12
            printLocation "| |   |     | | | | |         |", 20, 13
            printLocation "| |-- ----- | | | | |----------", 20, 14
            printLocation "| |       |     |         |     ", 20, 15
            printLocation "| ----------------------- |-- -", 20, 16
            printLocation "|         |             | |   |", 20, 17
            printLocation "|-------- | ----------- | | | |", 20, 18
            printLocation "|     |   | |           |   | |", 20, 19
            printLocation "| --- | --| | | | ------------|", 20, 20
            printLocation "| |   |     | | | |           |", 20, 21
            printLocation "| | | |   | | | --- ----- --- |", 20, 22
            printLocation "|   |   | |   |         |   | |", 20, 23
            printLocation "-------------------------------", 20, 24
            ret
        mazeDifficult endp  

        mazeMedium proc            ; Init point 30, 10
            printLocation "---------------------", 20, 0 
            printLocation "|     |             |", 20, 1
            printLocation "| | ------ -------- |", 20, 2
            printLocation "| |             | | |", 20, 3
            printLocation "| | |-----------|   |", 20, 4
            printLocation "| | |     |     | | |", 20, 5
            printLocation "| | | |-------| | | |", 20, 6
            printLocation "| |   |       |   | |", 20, 7
            printLocation "| |-| | |---| | | |-|", 20, 8
            printLocation "| | |-| |   | | |-| |", 20, 9                    
            printLocation "| | | |-|   |   | | |", 20, 10
            printLocation "| |   | |   | | | | |", 20, 11
            printLocation "| | | | |- -| | | | |", 20, 12        
            printLocation "| | | |       | |   |", 20, 13
            printLocation "| | | |---------|   |", 20, 14
            printLocation "| | |           | | |", 20, 15
            printLocation "|   |----- -----| | |", 20, 16
            printLocation "| |   |           | |", 20, 17
            printLocation "| |---------------- |", 20, 18
            printLocation "|   |               |", 20, 19
            printLocation "---------- ----------", 20, 20
            ret
        mazeMedium endp   

        mazeEasy proc            ; Init point 21, 3
            printLocation "------------- -", 20, 0 
            printLocation "|           | |", 20, 1
            printLocation "| --------- | |", 20, 2
            printLocation "|     |   | | |", 20, 3
            printLocation "| --- - - | | |", 20, 4
            printLocation "| | |   | | | |", 20, 5
            printLocation "| | |---- --- |", 20, 6
            printLocation "|       |     |", 20, 7
            printLocation "| ----- ----- |", 20, 8
            printLocation "|         |   |", 20, 9
            printLocation "---------------", 20, 10
            ret
        mazeEasy endp 

        youWin proc
            ;-------------------------- Catch final time ---------------------
                  call getHoursFinish
                  call getMinutesFinish
                  call getSecondsFinish

            clearDesktop
            printLocation " __      __                       __       __ __          __", 20, 0 
            printLocation "/  \    /  |                     /  |  _  /  /  |        / |", 20, 1
            printLocation "$$  \  /$$______  __    __       $$ | / \ $$ $$/ _______ $$|", 20, 2 
            printLocation " $$  \/$$/      \/  |  /  |      $$ |/$  \$$ /  /       \$$|", 20, 3 
            printLocation "  $$  $$/$$$$$$  $$ |  $$ |      $$ /$$$  $$ $$ $$$$$$$  $$|", 20, 4 
            printLocation "   $$$$/$$ |  $$ $$ |  $$ |      $$ $$/$$ $$ $$ $$ |  $$ $$/", 20, 5 
            printLocation "    $$ |$$ \__$$ $$ \__$$ |      $$$$/  $$$$ $$ $$ |  $$ |__", 20, 6  
            printLocation "    $$ |$$    $$/$$    $$/       $$$/    $$$ $$ $$ |  $$ / |", 20, 7 
            printLocation "    $$/  $$$$$$/  $$$$$$/        $$/      $$/$$/$$/   $$/$$/", 20, 8 

            ;----------------------------
              setCursorInPosition 25, 12
              PRINT "Player Movements: "
              call convertToDecimal        ; esto solo funciona con digitos hasta el 99 en decimal
            ;---------------------------

            endProgram
            
            ret
        youWin endp

        drawAndSetInitialPointEasy proc            ; Init point 21, 3       Final point 33, 0
            mov xToCheck, 21
            mov yToCheck, 02

            mov posX, 21
            mov posY, 3
            mov xBefore, 21
            mov yBefore, 3

            call mazeEasy

            ;-------------------- draw point into input and output -----------
                setSectionColorScreenTextMode 7ah, 21, 3, 21, 3
                setSectionColorScreenTextMode 3ah, 33, 0, 33, 0
            jmp go
            ret
        drawAndSetInitialPointEasy endp

        drawAndSetInitialPointMedium proc           ; Init point 30, 10     Final point 30, 20
            mov xToCheck, 30
            mov yToCheck, 09

            mov posX, 30
            mov posY, 10
            mov xBefore, 30
            mov yBefore, 10

            call mazeMedium

            ;-------------------- draw point into input and output -----------
                setSectionColorScreenTextMode 7ah, 30, 10, 30, 10
                setSectionColorScreenTextMode 3ah, 30, 20, 30, 20
            jmp go
            ret
        drawAndSetInitialPointMedium endp

        drawAndSetInitialPointDifficult proc            ; Init point 21, 23     Final point 51, 15
            mov xToCheck, 21
            mov yToCheck, 24

            mov posX, 21
            mov posY, 23
            mov xBefore, 21
            mov yBefore, 23

            call mazeDifficult

            ;-------------------- draw point into input and output -----------
                setSectionColorScreenTextMode 7ah, 21, 23, 21, 23
                setSectionColorScreenTextMode 3ah, 50, 15, 51, 15
            jmp go
            ret
        drawAndSetInitialPointDifficult endp                


        comprovePointWinEasy proc
            cmp dx, 0121h
                JE call youWin
            jmp go2
            ret
        comprovePointWinEasy endp

        comprovePointWinMedium proc
            cmp dx, 141Eh
                JE call youWin
            jmp go2
            ret
        comprovePointWinMedium endp

        comprovePointWinDifficult proc
            cmp dx, 0f32h
                JE call youWin
            jmp go2
            ret
        comprovePointWinDifficult endp

    ;----------------------------- Game procs --------------------------------------

        output proc
            mov dl, characterRead
            mov ah, 2h
            int 21h
            ret
        output endp

        input proc
            mov ah, 01h
            int 21h
            ret
        input endp

        convertToDecimal proc 
            mov al, playerMovements

            div decimalBase
            mov dx,ax
            or dx,3030h

            mov ah,02h
            int 21h

            xchg dh,dl
            mov ah,02h
            int 21h  
           
            ret
        convertToDecimal endp 

    ;------------------------- Procs to catch time ---------------------------------

        getHoursInit proc
            mov al, ch
            aam 
            add ax, 3030h
            mov HlInit, al
            mov HHInit, ah
            ret
        getHoursInit endp

        getMinutesInit proc
            mov al, cl
            aam
            add ax, 3030h
            mov MLInit, al
            mov MHInit, ah
            ret
        getMinutesInit endp 

        getSecondsInit proc
            mov al, dh
            aam
            add ax, 3030h
            mov SLInit, al
            mov SHInit, ah
            ret
        getSecondsInit endp

        getHoursFinish proc
            mov al, ch
            aam 
            add ax, 3030h
            mov HLFinish, al
            mov HHFinish, ah
            ret
        getHoursFinish endp

        getMinutesFinish proc
            mov al, cl
            aam
            add ax, 3030h
            mov MLFinish, al
            mov MHFinish, ah
            ret
        getMinutesFinish endp 

        getSecondsFinish proc
            mov al, dh
            aam
            add ax, 3030h
            mov SLFinish, al
            mov SHFinish, ah
            ret
        getSecondsFinish endp


        ;------------- test proc ------

           ; mov cx, 1
           ; mov si, 0

           ; ciclo:
           ;   cmp si, 0
           ;     je mov HInit[si], HHInit
           ;   cmp si, 1
           ;    je mov Hinit[si], HLInit 
           ;   inc si
           ; loop ciclo

    ;----------------------- Procs to move Character -------------------------------
  
        delBefCharacter proc
            mov  ah, 02h
            mov  dl, 32
            int  21h
            ret
        delBefCharacter endp


        setCursorBefPos proc
            mov ah, 02h
            mov bh, 00h  
            mov dh, yBefore
            mov dl, xBefore
            int 10h
            ret
        setCursorBefPos endp

        delCharBefPos proc
            mov ah, 02h
            mov dl, 20h        ; cod ascii 20h = "Space"
            int 21h
            ret
        delCharBefPos endp

        setCursorPosSelected proc
            mov ah, 02h
            mov bh, 00h 
            mov dh, posY
            mov dl, posX
            int 10h
            ret
        setCursorPosSelected endp              
        
        showCharacterInScreen proc
            mov ah, 02h
            mov dl, characterRead
            int 21h
            ret
        showCharacterInScreen endp

        savePosActual proc
            call setCursorPosSelected
            mov xBefore, dl
            mov yBefore, dh
            ret
        savePosActual endp

        savePosBef proc
            mov posX, dl
            mov posY, dh
            ret
        savePosBef endp


        waitCursor proc
            mov ah, 00h
            int 16h
            ret
        waitCursor endp


        readCharacterIntoScreen proc
            mov ah, 08h
            mov bh, 00h
            int 10h
            mov charIntoScreen, al
            ret 
        readCharacterIntoScreen endp

    ;------------------------------- Moves -----------------------------------------

        leftMove proc
            dec posX
            jmp next3
            ret
        leftMove endp

        rightMove proc
            inc posX
            jmp next3
            ret
        rightMove endp

        topMove proc
            inc posY
            jmp next3
            ret
        topMove endp

        bottomMove proc
            dec posY
            jmp next3
            ret
        bottomMove endp

    ;------------------------ SO procs (Console) ----------------------------------

        cls proc
            
            mov ah, 05h
            mov al, 00h
            int 10h

            ret
        cls endp

    ;-------------------------- Aparience procs ------------------------------------

        playMenuWithNameInputProc proc
            push ax
              playMenuWithNameInput userName
            pop ax
            jmp nextStep
            ret
        playMenuWithNameInputProc endp

        displayFirstMenu proc

              setSectionColorScreenTextMode 30h, 00, 00, 19, 24 

              setSectionColorScreenTextMode 5fh, 01, 04, 12, 04
              printLocation "1) PLAY!", 2, 4 

              setSectionColorScreenTextMode 5fh, 01, 12, 14, 12
              printLocation "2) STADISTICS", 2, 12

              ret

        displayFirstMenu endp

        displaySecondMenu proc

              setSectionColorScreenTextMode 5fh, 03, 06, 13, 06
              printLocation "1.NEW GAME", 4, 6

              setSectionColorScreenTextMode 5fh, 03, 08, 15, 08
              printLocation "2.SAVED GAME", 4, 8

              ret
        displaySecondMenu endp

        displayThirdMenu proc
            ;---------------------- clean the last menu --------------------
              ; set the chosen option

                setSectionColorScreenTextMode 30h, 00, 00, 19, 24 

                setSectionColorScreenTextMode 5fh, 01, 04, 12, 04
                printLocation "1) PLAY!", 2, 4 

                setSectionColorScreenTextMode 5fh, 03, 06, 13, 06
                printLocation "1.NEW GAME", 4, 6

            ;--------------------------------------------------------------    
              ;choose the menu of levels  

              setSectionColorScreenTextMode 5fh, 05, 08, 15, 08
              printLocation "1. LEVEL 1", 6, 8

              setSectionColorScreenTextMode 5fh, 05, 10, 15, 10
              printLocation "2. LEVEL 2", 6, 10

              setSectionColorScreenTextMode 5fh, 05, 12, 15, 12
              printLocation "3. LEVEL 3", 6, 12

              printLocation "CHOOSE THE LEVEL: ", 22, 1

              ret
        displayThirdMenu endp

;============================= END PROCEDURES ====================================                               
