
;================================ MACROS =========================================        

  setCursorInInitGamePosition macro x, y
      mov dl, x
      mov dh, y

      mov posX, dl
      mov posY, dh
      
      mov ah, 02h
      mov bh, 00h 
      int 10
  endm

  getCursorPosition macro
      mov ah, 03h       ; get cursor position
      mov bh, 00h
      int 10h
  endm



  setCursorInPosition macro x, y
      mov ah, 02h
      mov bh, 00h
      mov dh, y
      mov dl, x
      int 10h

  endm

  printMsg macro msg          ; Macro para imprimir mensajes
      mov ah, 09h
      mov dx, offset msg
      int 21h
  endm       

  printChar macro char          ; Macro para chars
      mov ah, 02h
      mov dl, char
      int 21h
  endm        

  printLocation macro text, x, y
      mov ah, 02h
      mov bh, 00h
      mov dh, y
      mov dl, x
      int 10h

      PRINT text

  endm

  setSectionColorScreenTextMode macro cText, xInit, yInit, xFinal, yFinal
      mov ax, 0600h
      mov bh, cText

      mov ch, yInit 
      mov cl, xInit

      mov dh, yFinal
      mov dl, xFinal

      int 10h
  endm

  endProgram macro
      mov ax, 0c07h
      int 21h
      .exit
  endm 

  set_Video_mode_80x25_03h macro    ; Text mode setting
      mov ah, 00h
      mov al, 03h
      int 10h  
  endm
      
      
  set_Video_mode_80x30_12h macro ; Graphics mode setting
      mov ah, 00h
      mov al, 12h
      int 10h  
  endm    
      

  clearDesktop macro 
      mov ah, 06h
      mov al, 00h

      mov bh, 0fh

      mov ch, 00
      mov cl, 20

      mov dh, 24
      mov dl, 79

      int 10h 
  endm

    ;-------------------------- Menu Macro --------------------------------------

      ;---------------------- Option 1 ------------------------------------------

      playMenuWithNameInput macro userName
        ;---------------------- clean the last menu --------------------
        ; set the chosen option

          setSectionColorScreenTextMode 30h, 00, 00, 19, 24 

          setSectionColorScreenTextMode 5fh, 01, 04, 12, 04
          printLocation "1) PLAY!", 2, 4 
        ;--------------------------------------------------------------

          clearDesktop

          printLocation "Type your Name: ", 22, 1

          ;----------------- GET THE NAME AND SAVE IT-----------------

            MOV CX, 15    ;extencion of the word is 15 charters
            MOV SI, 0
            
            L:
              MOV AH,01h
              INT 21H
              CMP AL,0DH ;compare with an enter key
              JE endE    ;if equals jumpo to the end
              MOV userName[SI],AL    
              INC SI 
            loop L
            endE:

      endm

;=============================== END MACROS ====================================== 